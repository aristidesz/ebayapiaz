# coding=utf-8
# encoding=utf8 
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
import template
from common import dump
import credentials_ebay as ce
import api_requests as ebay_api
from kzalez_API.Item import Item
from kzalez_API.ImageSelection import ImageSelection
import glob, os
import sys

sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()





itemID = ''
shippingprice = '0.0'
mynote = "ShippingPrice,"+str(shippingprice)
#Set Note For shipping Price (Used by SKU GRID)
try:
	api = ebay_api.connectTradingReal()
	callData = {
		"Action": "AddOrUpdate",
		"ItemID": itemID,
		"NoteText": "ShippingPrice,"+str(shippingprice)
	}
	response = api.execute('SetUserNotes', callData)
except ConnectionError as e:
	print e.response.reply
	sys.exit(0)