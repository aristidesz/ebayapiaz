# coding=utf-8
# encoding=utf8 
import datetime
from amazonproduct import API
import os
import time
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
import template
from common import dump
import credentials_ebay as ce
import api_requests as ebay_api
from kzalez_API.Item import Item
from kzalez_API.ImageSelection import ImageSelection
import glob, os
from lxml import etree
from priceCalc import calculatePrice
import random
import resize_images
import sys,shutil
import glob, os
import urllib
import csvReader
from xml.sax.saxutils import escape
import xml.etree.ElementTree as ET

sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()


cwd = os.getcwd()
serverUrl = '/home/ubuntu/ebay/stable_running/'



def fetchAmazonDetails(results,title):
	# results = result = api.item_lookup(ItemId=fetchedAsin,ResponseGroup='Large')
	# # print etree.tostring(results, pretty_print=True)

	# fopen = open('output.xml','w')
	# fopen.write(etree.tostring(results, pretty_print=True))
	# fopen.close()
	# fopen = open('data/output'+ str(urlOrAssin) +'.xml','w')
	# fopen.write(etree.tostring(results, pretty_print=True))
	# fopen.close()
	# # import pdb; pdb.set_trace()

	# title = results.Items.Item.ItemAttributes.Title
	logInitialTitle = results.Items.Item.ItemAttributes.Title


	try:
		brand=results.Items.Item.ItemAttributes.Brand
	except Exception as e:
		print "Error occurred!"
		print e
		brand = "N/A"

	feature_bullets = []
	try:
		print "Product Description: \n"
		for i in results.Items.Item.ItemAttributes.Feature:
			try:
				print i
			except Exception as e:
				print e
			feature_bullets.append(i)
	except Exception as e:
		print e
		feature_bullets = ""
		pass
	# feature_bullets = 
	# print feature_bullets
	try:
		product_description = results.Items.Item.EditorialReviews.EditorialReview.Content
		try:
			print "\n"+product_description +"\n"
		except Exception as e:
			print e
	except Exception as e:
		product_description = ""
		print e
		pass

	try:
		price = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
	except Exception as e:
		price = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01

	initialPrice = price

	# try:
	# 	# price = float(results.Items.Item.OfferSummary.LowestNewPrice.Amount)*0.01 #Offer Price
	# 	price = float(results.Items.Item.ItemAttributes.ListPrice.Amount)*0.01
	# except:
	# 	price = float(results.Items.Item.ItemAttributes.ListPrice.Amount)*0.01
	print "Price: \n"+str(price)+"\n"

	# flag = True
	# shippingValue = float(results.Items.Item.Offers.Offer.OfferListing.IsEligibleForSuperSaverShipping)
	# if shippingValue == 0:
	shippingPrice = 0
		# print "Free Shipping Detected"
		# shippingPrice=float(raw_input('Enter a shipping cost:'))
	# else:
	# 	while flag:
	# 		shippingPrice=float(raw_input('Please enter shipping cost:'))
	# 		flag = False


	print "Shipping Price: \n"+ str(shippingPrice) + "\n"
	# finalprice = ((price+shippingPrice)*(0/100+1)+(((price+shippingPrice)*(0/100+1)*5.8/100+0.3)+0.2+abs(((price+shippingPrice)*(0/100+1)*5.8/100+0.3)-0.2))/2+0.2)/((100-9-3.4)/100)
	finalprice = calculatePrice(price,shippingPrice)
	# import pdb; pdb.set_trace()
	print "Final Ebay Price: \n" + str(finalprice) + "\n"

	# import pdb;pdb.set_trace()
	title_words = str(title).split(" ")

	temp_image = []
	# img_lst = []
	# print "Images: \n"
	try:
		temp_image.append(results.Items.Item.LargeImage.URL)
		fst_image = results.Items.Item.LargeImage.URL
	except Exception as e:
		print e
	for x in results.Items.Item.ImageSets.ImageSet:
		# print x.LargeImage.URL + "\n"
		temp_image.append(x.LargeImage.URL)
		image_lst = list(set(temp_image))

	# import pdb;pdb.set_trace()
	#Bring Main Image in Front
	try:
		image_lst.insert(0, image_lst.pop(image_lst.index(fst_image)))
	except Exception as e:
		print e

	# try:
		# print image_lst
	image_names_lst = []
	img_counter = 0
	for im in image_lst:	
		word1 = random.randrange(0, len(title_words)) 
		word2 = random.randrange(0, len(title_words)) 
		image_word1 = title_words[word1]
		image_word2 = title_words[word2]
		image_names = image_word1+"-"+image_word2+".jpg"
		img_counter += 1
		image_names_lst.append(image_names)
		urllib.urlretrieve(str(im), image_names)
	# except Exception as e:
	# 	print e
	# import pdb;pdb.set_trace()

	for im in image_names_lst:
		print im
		resize_images.resize_img(im)

	flagImages = True
	image_urls = []

	while flagImages==True:
		try:
			api = ebay_api.connectTradingReal()

			# pass in an open file
			# the Requests module will close the file
			for imid in image_names_lst:
				files = {'file': ('EbayImage', open(str(imid), 'rb'))}
				pictureData = {
					"WarningLevel": "High",
					"PictureName": str(imid)
				}
				response = api.execute('UploadSiteHostedPictures', pictureData, files=files)
				image_urls.append(response.reply.SiteHostedPictureDetails.FullURL)
				# print response.reply.SiteHostedPictureDetails.FullURL
			flagImages = False

		except ConnectionError as e:
			print(e)
			print(e.response.dict())
			flagImages = True

	if len(image_urls) < 9:
		# image_urls.append("http://i.ebayimg.com/00/s/NDQwWDUxMA==/z/HRUAAOSwzgBY3C19/$_1.PNG?set_id=8800005007")
		image_urls.append("http://i.ebayimg.com/00/s/NDg0WDU0Mg==/z/0mMAAOSwmgJY3C17/$_1.PNG?set_id=8800005007")
		image_urls.append("http://i.ebayimg.com/00/s/NzQxWDE1Nzc=/z/04EAAOSwDmBY3C1~/$_1.JPG?set_id=8800005007")

	image_urls = image_urls[:12]


	try:
		mpnc = results.Items.Item.ItemAttributes.Model
	except Exception as e:
		mpnc = "Does Not Apply"
		print e

	try:
		itemsize = results.Items.Item.ItemAttributes.Model
	except Exception as e:
		itemsize = "Does Not Apply"
		print e

	# import pdb; pdb.set_trace()

	category_lst = []
	category_parent_lst = []

	try:
		api = ebay_api.connectTradingReal()

		callData = {
			'Query': str(title),
		}

		response = api.execute('GetSuggestedCategories', callData)

		category_lst = []
		category_parent_lst = []
		for i in response.reply.SuggestedCategoryArray.SuggestedCategory:
			# category_lst.append(i.Category.CategoryParentID)
			category_lst.append(i.Category.CategoryID)
	except ConnectionError as e:
		print(e)
		print(e.response.dict())
		print e.response.reply.Errors.ShortMessage

	# import pdb; pdb.set_trace()
	product_category = category_lst[0]
	print "Product Category: \n" + product_category + "\n"

	# print "ShippingPrice: \n" + str(shippingPrice) + "\n"
	return title,brand,feature_bullets,product_description,product_category,finalprice,image_urls,shippingPrice,logInitialTitle,initialPrice,mpnc


def execute_sku_grid():

	def checkInvalidAsin(asin,itemid):
		print "Listing with ASIN " +asin+" and itemID " +itemid +" is about to End. \n"

		#Counter log for ending listings
		fread = open(cwd+'/counters/%s.txt' % asin,'r')

		counterLog = fread.readline()
		counterLog = int(counterLog)


		if counterLog < 12:
			print counterLog
		else:
			# import pdb; pdb.set_trace()
			print counterLog
			print "Remove the item"
			# endMyListing(itemid)	
			return False
		fread.close()

		fopen = open(cwd+'/counters/%s.txt' % asin, 'w')
		counterIter = counterLog + 1

		fopen.write('%d' % counterIter)
		fopen.close()
		flagEndListing = False

	def endMyListing(itemid):
		try:
			api = ebay_api.connectTradingReal()
			myitem = {
				"ItemID": i,
				"EndingReason": "Incorrect"
			}
			response = api.execute('EndFixedPriceItem', myitem)
			print "Listing with ItemID "+str(itemid)+" has ended because ASIN not found! \n"
			# import pdb; pdb.set_trace()
		except ConnectionError as e:
			print e.response.reply.Errors
			print "\n"



	def search_asin(asin,itemid,title):
		maxRetries = 0
		api = API(locale='uk')
		flag = True
		while flag == True:
			try:
				results = api.item_lookup(ItemId=asin,ResponseGroup='Large')
				time.sleep(0.5)
				flag = False
				maxRetries = 0
				# import pdb; pdb.set_trace()

				try:
					fopen = open(cwd+'/data/output'+ str(asin) +'.xml','w')
					fopen.write(etree.tostring(results, pretty_print=True))
					fopen.close()
				except Exception as e:
					print e

			except Exception as e:
				print e
		
				if str(e) == "AWS.InvalidParameterValue: "+str(asin)+" is not a valid value for ItemId. Please change this value and retry your request.":
					print "Invalid Asin" 
					flagOut = checkInvalidAsin(asin,itemid)
					if flagOut == False:
						return str(100),1
				else:
					maxRetries += 1
					if maxRetries == 5:
						time.sleep(20.0)
					results = None
					time.sleep(2.0)
					flag = True


		finalprice = None
		'''
		try:
			finalprice = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
		except Exception as e:
			finalprice = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01
		'''
		flagPrice = False
		flagSalePrice = False
		flagEndListing = True


		try:
			title,brand,feature_bullets,product_description,product_category,finalprice,image_urls,shippingPrice,logInitialTitle,initialPrice,mpnc = fetchAmazonDetails(results,title)
		except Exception as e:
			print "Waaarniiing!"
			print e
			import pdb;pdb.set_trace()

		try:
			finalprice = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
			flagSalePrice = True
		except Exception as e:
			# print e
			try:
				finalprice = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01
				flagPrice = True
			except Exception as e:
				print e
				priceExist = isinstance(finalprice, float)
				if priceExist == False and flagPrice == False and flagSalePrice == False:
					print "Listing with ASIN " +asin+" and itemID " +itemid +" is about to End. \n"
					

					#Counter log for ending listings
					fread = open(cwd+'/counters/%s.txt' % asin,'r')

					counterLog = fread.readline()
					counterLog = int(counterLog)


					if counterLog < 12:
						print counterLog
					else:
						# import pdb; pdb.set_trace()
						print counterLog
						print "Remove the item"
						# endMyListing(itemid)	

					fread.close()

					fopen = open(cwd+'/counters/%s.txt' % asin, 'w')
					counterIter = counterLog + 1

					fopen.write('%d' % counterIter)
					fopen.close()
					flagEndListing = False

					#-------------

					#import pdb; pdb.set_trace()
		# try:
		# 	finalprice = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
		# except Exception as e:
		# 	finalprice = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01
		
		# else:
		# 	import pdb; pdb.set_trace()
		# 	endMyListing(itemid)
		# # print float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01, float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
		myflag = 0
		if results == None or finalprice==None:
			print "Final Price found as None"
			myflag = 1

			
		# results = api.item_lookup(ItemId=asin,ResponseGroup='Large')
			# time.sleep(15.0)
		# priceNormal = float(results.Items.Item.ItemAttributes.ListPrice.Amount)*0.01
		# priceOffer = float(results.Items.Item.OfferSummary.LowestNewPrice.Amount)*0.01
		title = results.Items.Item.ItemAttributes.Title 
		# print title+ "\n"
		if flagEndListing == True:
			fopen = open(cwd+'/counters/%s.txt' % asin, 'w')
			counterIter = 0

			fopen.write('%d' % counterIter)
			fopen.close()


		available = True
		try:
			availability = results.Items.Item.Offers.Offer.OfferListing.Availability

			if str(availability) == "Usually dispatched within 1 to 2 months" or str(availability) == "Usually dispatched within 1 to 4 weeks" or str(availability) == "Usually dispatched within 4-5 business days":
				print "Usually dispatched within 1 to 2 months"
				available = False
		except Exception as e:
			print e

		return str(finalprice),myflag,available,title,brand,feature_bullets,product_description,product_category,image_urls,shippingPrice,logInitialTitle,initialPrice,mpnc
		
	title_lst = []
	shippingPrice_lst = []
	itemID_lst = []
	asin_lst = []
	buyit_lst = []
	qty_lst = []
	counterActiveList = 0
	flagFetchItems = True
	pagesCounter = 1
	while flagFetchItems:
		try:
			api = ebay_api.connectTradingReal()
			myitem = {
				"ActiveList": {
					"Include":"True",
						"Pagination":{
							"EntriesPerPage":"200",
							"PageNumber":str(pagesCounter)
					}
				}
			}

			response = api.execute('GetMyeBaySelling', myitem)
			pagesCounter += 1


			counter_items = 0
			for i in response.reply.ActiveList.ItemArray.Item:

				try:
					# print i.SKU
					shipPrice = i.SKU.split(",")[1]
					# import pdb; pdb.set_trace()
					shippingPrice_lst.append(shipPrice)
					itemID_lst.append(i.ItemID)
					title_lst.append(i.Title)
					asin_lst.append(i.SKU.split(",")[0])
					buyit_lst.append(i.BuyItNowPrice.value)
					# import pdb; pdb.set_trace()
					qty_lst.append(i.QuantityAvailable)
					counter_items +=1
					counterActiveList += 1
					# import pdb; pdb.set_trace()
				except Exception as e:
					# print e
					# pass
					continue
		except ConnectionError as e:
			print e.response.reply.Errors

		if counter_items != 200:
			flagFetchItems = False
	print "No. of Items found in Active List is: " + str(counterActiveList)
	# import pdb;pdb.set_trace()

	counterRetry = 1

	# counterRetry_lst = [x for x in xrange(18,20000,15)]
	for i,j,z,sh,qty,title in zip(itemID_lst,asin_lst,buyit_lst,shippingPrice_lst,qty_lst,title_lst):
		print "No. of Items Checked: "+ str(counterRetry)


		# time.sleep(12.0)
		# if counterRetry < 40:
		# 	counterRetry +=1
		# 	continue
		# else:
		# 	counterRetry +=1

		counterRetry += 1

		# if counterRetry < 705:
		# 	continue

		priceOffer,myflag,avail,title,brand,feature_bullets,product_description,product_category,images_url,shippingPrice,logInitialTitle,initialPrice,mpnc = search_asin(j,i,title)
		if myflag == 1:
			continue


		if avail == True:
			itemavail = "1"

		if avail == False:
			itemavail = "0"


		draft_offer = priceOffer
		shippingPrice = float(sh)
		# priceOffer = ((float(priceOffer)+shippingPrice)*(0/100+1)+(((float(priceOffer)+shippingPrice)*(0/100+1)*5.8/100+0.3)+0.2+abs(((float(priceOffer)+shippingPrice)*(0/100+1)*5.8/100+0.3)-0.2))/2+0.2)/((100-9-3.4)/100)
		priceOffer = calculatePrice(float(priceOffer),shippingPrice)
		priceOffer = "%.2f" % priceOffer
		ebayPrice = z

		#Check Last Digit if zero
		if priceOffer[-1:] == '0':
			priceOffer= priceOffer[:-1]
		# print ebayPrice,priceOffer

		# import pdb; pdb.set_trace()
		# if ebayPrice == priceOffer and qty == itemavail:
		# 	print "No change in price detected"
		# 	print datetime.datetime.now()
		# 	print "ASIN: " + str(j) + "\n"
		# else:
		title = str(title)[:80]

		# import pdb; pdb.set_trace()
		import sys  

		reload(sys)  
		sys.setdefaultencoding('utf8')

		# final_price = "%.2f" % final_price

		# product_description = product_description
		# print title,product_description,category_Id,final_price,images_url

		bf = ""

		if feature_bullets!= "":
			for bu in feature_bullets:
				bu = "<li>" + bu + "</li>"
				bf = bf + bu
		
		feature_bullets = "<ul>" + bf + "</ul>"

		# print feature_bullets
		if len(images_url) > 1:
			main_image = images_url[0]
		else:
			main_image = images_url[0]
			print "One image detected!"
		# print "<![CDATA[" + template.prod_desc(title,main_image,feature_bullets,product_description) + "Z]]>"
		
		# product_description = "<![CDATA[" + template.prod_desc(title,main_image,feature_bullets,product_description) + "]]>"
		product_description =  template.prod_desc(title,main_image,feature_bullets,product_description) 
		
		mpn = "N/A"
		# import pdb; pdb.set_trace()
		mpn = csvReader.returnEanDict(str(j))
		# import pdb;pdb.set_trace()
		brand = escape(str(brand))

		print brand,mpn


		# import pdb; pdb.set_trace()

		myURLs = ','.join(map(str, images_url)) 

		print "Revising ItemID "+str(i)+ " with ASIN " + j
		print "Price: \nListed Price "+z+", Offer Amazon Price "+str(draft_offer)+ ", Ebay Price After Offer "+ str(priceOffer)+"\n"
		try:
			api = ebay_api.connectTradingReal()



			myitem = {
				"Item": {
					"ItemID": i,
					# "Description": str(product_description),
					# "PrimaryCategory": {"CategoryID": str(product_category)},
					# "StartPrice": str(priceOffer),

					"PayPalEmailAddress": "reliablesales101@gmail.com",
	
					# "Quantity": itemavail,
					"AutoPay": 'True',
					"ItemSpecifics":{
						"NameValueList":
						[
						  {
						    "Name": "Brand",
						    "Value": brand
						  },
						  {
						    "Name": "MPN",
						    "Value": mpnc
						  },
						  {
						  	"Name": "UPC",
						  	"Value": mpn
						  }
						]},
					"ProductListingDetails":{
						"EAN":mpn
					}
				}
			}


			response = api.execute('ReviseFixedPriceItem', myitem)
			print datetime.datetime.now()
			print "Price Changed \n"
			# import pdb; pdb.set_trace()
		except ConnectionError as e:
			print e.response.reply.Errors
			print datetime.datetime.now()
			print "\n"
		for file in glob.glob("*.jpg"):
		# print(file)
			os.remove(file)


		# import pdb;pdb.set_trace()

if __name__ == '__main__':
	execute_sku_grid()
