
size = 500, 500

import PIL
from PIL import Image,ImageOps
def resize_img(name):

	# basewidth = 500
	# img = Image.open(name)
	# wpercent = (basewidth/float(img.size[0]))
	# hsize = int((float(img.size[1])*float(wpercent)))
	# img = img.resize((basewidth,hsize), PIL.Image.ANTIALIAS)
	# img.save(str(name)) 


	old_im = Image.open(name)
	old_size = old_im.size
	new_im = old_im
	if old_size[0] < 500:
		new_size = (800, 800)
		new_im = Image.new("RGB", new_size)   ## luckily, this is already black!
		new_im = ImageOps.expand(old_im,border=(500-old_size[0]),fill='white')
		# new_im.paste(old_im, ((new_size[0]-old_size[0])/2,
		#                       (new_size[1]-old_size[1])/2))
	new_im.save(name)

if __name__ == '__main__':
	resize_img("myimg.jpeg")