** Python Ebay API scripts
---

## Scripts to:

1. Add item to Ebay store.
2. Get messages send/received from/to Ebay store.
3. Get traffic of Ebay store.
4. Get number of sold items per Ebay store.
5. Calculate price based on Amazon price.
6. Update prices based on Amazon price fluctuations.
7. Revise items on Ebay store.
8. Check items that fall in VERO category.
9. Remove items from Ebay store.
