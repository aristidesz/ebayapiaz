

def calculatePrice(price,shippingPrice):
	vendorTax = 0
	margin = 4.0
	fixedMargin = 0.1
	minimumMargin = 0.2
	payPalAmazonFeesPerc = 3.4
	payPalAmazonFees = 0.2
	sellingOther = 9

	finalPrice = ((min(max((price-(0-0.00001))*100000, 0), 1)*min(max(((99999999-0.00001)-price)*100000, 0), 1))*(((price+shippingPrice)*(0/100+1)+(((price+shippingPrice)*(0/100+1)*margin/100+fixedMargin)+0.2+abs(((price+shippingPrice)*(0/100+1)*margin/100+fixedMargin)-payPalAmazonFees))/2+minimumMargin)/((100-sellingOther-payPalAmazonFeesPerc)/100)))
	# finalprice = ((price+shippingPrice)*(0/100+1)+(((price+shippingPrice)*(0/100+1)*5.8/100+0.3)+0.2+abs(((price+shippingPrice)*(0/100+1)*5.8/100+0.3)-0.2))/2+0.2)/((100-9-3.4)/100)
	return finalPrice

if __name__ == '__main__':
	try:
		price=str(raw_input('Insert Price:'))
	except ValueError:
		print "Error occurred!"
	try:
		shippingPrice=str(raw_input('Insert Shipping:'))
	except ValueError:
		print "Error occurred!"

	price = float(price)
	shippingPrice = float(shippingPrice)
	finalPrice = calculatePrice(price,shippingPrice)

	print finalPrice
