# coding=utf-8
# encoding=utf8 
import datetime
from amazonproduct import API
import os
import time
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
import template
from common import dump
import credentials_ebay as ce
import api_requests as ebay_api
from kzalez_API.Item import Item
from kzalez_API.ImageSelection import ImageSelection
import glob, os
from lxml import etree

sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()




		
def getMessages():
	try:
		api = ebay_api.connectTradingReal()

		# now = datetime.datetime.now()

		# dt_stop = datetime.datetime.now()
		# dt_start = dt_stop - datetime.timedelta(days=7)

		dt_start = datetime.datetime.strptime("01/4/18 16:30", "%d/%m/%y %H:%M")
		dt_stop = datetime.datetime.strptime("21/4/18 16:30", "%d/%m/%y %H:%M")

		memberData = {
			"DetailLevel":"ReturnSummary",
			"MailMessageType": "All",
			# "MessageStatus": "Unanswered",
			"StartTime" : dt_start,
			"EndTime": dt_stop,
			"Pagination": {
				"EntriesPerPage": "5",
				"PageNumber": "1"
			}
		}

		api.execute('GetMyMessages', memberData)
		print api.response.reply.Summary.TotalMessageCount
		# dump(api)

		if api.response.reply.has_key('Messages'):
			messages = api.response.reply.MemberMessage.MemberMessageExchange
			# print messages
			print "Number of messages is: " + str(len(messages))

			if type(messages) != list:
				messages = [messages]

			# print messages
			for m in messages:
				print("%s: %s" % (m.CreationDate, m.Question.Subject))

	except ConnectionError as e:
		print(e)
		print(e.response.dict())

if __name__ == '__main__':
	getMessages()