from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
from ebaysdk.shopping import Connection as Shopping
from ebaysdk.merchandising import Connection as merchandising

import credentials_ebay as ce



sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()

token_sandbox = ""
token = ""
def connect():
	api = Connection(domain='svcs.sandbox.ebay.com',appid=sbAppId, config_file='myebay.yaml')
	return api
def connectShopping():
	api = Shopping(domain='open.api.sandbox.ebay.com',appid=sbAppId, config_file='myebay.yaml')
	return api
def connectMerchandising():
	api = merchandising(appid=sbAppId, config_file='myebay.yaml')
	return api
def connectMerchandisingReal():
	api = merchandising(domain='open.api.ebay.com:',appid=prAppId, config_file='myebay.yaml')
	return api
def connectShoppingReal():
	api = Shopping(domain='open.api.ebay.com:',appid=prAppId, config_file='myebay.yaml')
	return api
def connectFindingReal():
	# domain='svcs.sandbox.ebay.com',
	api = Connection(appid=prAppId, config_file='myebay.yaml')
	return api
def connectTrading():
	api = Trading(domain='api.sandbox.ebay.com',appid=sbAppId, devid=sbDevId, certid=sbCertId, token=token_sandbox,config_file=None)
	return api
def connectTradingReal():
	api = Trading(appid=prAppId, devid=prDevId, certid=prCertId, token=token,config_file=None)
	return api