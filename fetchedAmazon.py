# coding=utf-8
# encoding=utf8 
from amazonproduct import API
import os
import time
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
from lxml import etree
from add_item import addFixedPriceItem
from common import dump
import credentials_ebay as ce
import api_requests as ebay_api
from kzalez_API.Item import Item
from kzalez_API.ImageSelection import ImageSelection
import glob, os
import urllib
import random
import resize_images
import sys,shutil
from priceCalc import calculatePrice
from xml.sax.saxutils import escape
import xml.etree.ElementTree as ET

sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()


if os.path.exists('Listings'):
	pass
else:
	os.makedirs('Listings')


api = API(locale='uk')
time.sleep(0.9)



try:
	urlOrAssin=str(raw_input('Insert ASIN:'))
except ValueError:
	print "Error occurred!"

check_asin_lst = []
try:
	# open csv file
	with open('Listings/MyListings.csv', 'rb') as csvfile:

		# get number of columns
		for line in csvfile.readlines():
			array = line.split(',')
			check_asin_lst.append(array[0])
		if urlOrAssin in check_asin_lst:
			print "\nWarning ASIN was already found in listed products!!!\n"
except Exception as e:
	print e

# api = API(locale='...')
results = result = api.item_lookup(ItemId=urlOrAssin,ResponseGroup='Large')
# print etree.tostring(results, pretty_print=True)

fopen = open('output.xml','w')
fopen.write(etree.tostring(results, pretty_print=True))
fopen.close()
fopen = open('data/output'+ str(urlOrAssin) +'.xml','w')
fopen.write(etree.tostring(results, pretty_print=True))
fopen.close()
# import pdb; pdb.set_trace()

title = results.Items.Item.ItemAttributes.Title
logInitialTitle = title

try:
	print "Initial title: " + str(logInitialTitle) + "\n"
except Exception as e:
	print "Error occurred!"
	print e

try:
	title=str(raw_input('Insert Monetized Title:'))
except Exception as e:
	print "Error occurred!"
	print e




print "Title: \n"+title + "\n"

try:
	brand=results.Items.Item.ItemAttributes.Brand
except Exception as e:
	print "Error occurred!"
	print e
	brand = "N/A"

try:
	brand = escape(str(brand))
except:
	brand = "N/A"
	
feature_bullets = []
try:
	print "Product Description: \n"
	for i in results.Items.Item.ItemAttributes.Feature:
		try:
			print i
		except Exception as e:
			print e
		feature_bullets.append(i)
except Exception as e:
	print e
	feature_bullets = ""
	pass
# feature_bullets = 
# print feature_bullets
try:
	product_description = results.Items.Item.EditorialReviews.EditorialReview.Content
	try:
		print "\n"+product_description +"\n"
	except Exception as e:
		print e
except Exception as e:
	product_description = ""
	print e
	pass

try:
	price = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
except Exception as e:
	price = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01

initialPrice = price

# try:
# 	# price = float(results.Items.Item.OfferSummary.LowestNewPrice.Amount)*0.01 #Offer Price
# 	price = float(results.Items.Item.ItemAttributes.ListPrice.Amount)*0.01
# except:
# 	price = float(results.Items.Item.ItemAttributes.ListPrice.Amount)*0.01
print "Price: \n"+str(price)+"\n"

flag = True
shippingValue = float(results.Items.Item.Offers.Offer.OfferListing.IsEligibleForSuperSaverShipping)
if shippingValue == 0:
	shippingPrice = 0
	print "Free Shipping Detected"
	shippingPrice=float(raw_input('Enter a shipping cost:'))
else:
	while flag:
		shippingPrice=float(raw_input('Please enter shipping cost:'))
		flag = False


print "Shipping Price: \n"+ str(shippingPrice) + "\n"
# finalprice = ((price+shippingPrice)*(0/100+1)+(((price+shippingPrice)*(0/100+1)*5.8/100+0.3)+0.2+abs(((price+shippingPrice)*(0/100+1)*5.8/100+0.3)-0.2))/2+0.2)/((100-9-3.4)/100)
finalprice = calculatePrice(price,shippingPrice)
# import pdb; pdb.set_trace()
print "Final Ebay Price: \n" + str(finalprice) + "\n"


title_words = str(title).split(" ")

temp_image = []
# img_lst = []
# print "Images: \n"
try:
	temp_image.append(results.Items.Item.LargeImage.URL)
	fst_image = results.Items.Item.LargeImage.URL
except Exception as e:
	print e
for x in results.Items.Item.ImageSets.ImageSet:
	# print x.LargeImage.URL + "\n"
	temp_image.append(x.LargeImage.URL)
	image_lst = list(set(temp_image))


#Bring Main Image in Front
try:
	image_lst.insert(0, image_lst.pop(image_lst.index(fst_image)))
except Exception as e:
	print e

# print image_lst
image_names_lst = []
img_counter = 0
for im in image_lst:	
	word1 = random.randrange(0, len(title_words)) 
	word2 = random.randrange(0, len(title_words)) 
	image_word1 = title_words[word1]
	image_word2 = title_words[word2]
	image_names = image_word1+"-"+image_word2+".jpg"
	img_counter += 1
	image_names_lst.append(image_names)
	urllib.urlretrieve(str(im), image_names)


for im in image_names_lst:
	print im
	resize_images.resize_img(im)

flagImages = True
image_urls = []

while flagImages==True:
	try:
		api = ebay_api.connectTradingReal()

		# pass in an open file
		# the Requests module will close the file
		for imid in image_names_lst:
			files = {'file': ('EbayImage', open(str(imid), 'rb'))}
			pictureData = {
				"WarningLevel": "High",
				"PictureName": str(imid)
			}
			response = api.execute('UploadSiteHostedPictures', pictureData, files=files)
			image_urls.append(response.reply.SiteHostedPictureDetails.FullURL)
			# print response.reply.SiteHostedPictureDetails.FullURL
		flagImages = False

	except ConnectionError as e:
		print(e)
		print(e.response.dict())
		flagImages = True

if len(image_urls) < 9:
	# image_urls.append("http://i.ebayimg.com/00/s/NDQwWDUxMA==/z/HRUAAOSwzgBY3C19/$_1.PNG?set_id=8800005007")
	image_urls.append("http://i.ebayimg.com/00/s/NDg0WDU0Mg==/z/0mMAAOSwmgJY3C17/$_1.PNG?set_id=8800005007")
	image_urls.append("http://i.ebayimg.com/00/s/NzQxWDE1Nzc=/z/04EAAOSwDmBY3C1~/$_1.JPG?set_id=8800005007")

image_urls = image_urls[:12]



# import pdb; pdb.set_trace()

category_lst = []
category_parent_lst = []

try:
	api = ebay_api.connectTradingReal()

	callData = {
		'Query': str(title),
	}

	response = api.execute('GetSuggestedCategories', callData)

	category_lst = []
	category_parent_lst = []
	for i in response.reply.SuggestedCategoryArray.SuggestedCategory:
		# category_lst.append(i.Category.CategoryParentID)
		category_lst.append(i.Category.CategoryID)
except ConnectionError as e:
	print(e)
	print(e.response.dict())
	print e.response.reply.Errors.ShortMessage

# import pdb; pdb.set_trace()
product_category = category_lst[0]
print "Product Category: \n" + product_category + "\n"

try:
	mpnc = results.Items.Item.ItemAttributes.Model
	numFlag = str(mpnc).isdigit()
	if numFlag == True:
		if int(mpnc) == 1 or int(mpnc) == 3 or int(mpnc) == 9 or int(mpnc) == 2 or int(mpnc) == 4:
			mpnc = "Does Not Apply"	
except Exception as e:
	mpnc = "Does Not Apply"
	print e

print mpnc


addFixedPriceItem(title,brand,feature_bullets,product_description,product_category,finalprice,image_urls,urlOrAssin,shippingPrice,logInitialTitle,initialPrice,mpnc)


