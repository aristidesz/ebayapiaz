# coding=utf-8
# encoding=utf8 
import datetime
from amazonproduct import API
import os
import time
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
import template
from common import dump
import credentials_ebay as ce
import api_requests as ebay_api
from kzalez_API.Item import Item
from kzalez_API.ImageSelection import ImageSelection
import glob, os
from lxml import etree
from priceCalc import calculatePrice

sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()


cwd = os.getcwd()
# serverUrl = '/home/ubuntu/ebay/stable_running/'
# cwd = '/home/ubuntu/ebay/stable_running/'


def execute_sku_grid():

	def checkInvalidAsin(asin,itemid):
		print "Listing with ASIN " +asin+" and itemID " +itemid +" is about to End. \n"

		#Counter log for ending listings
		fread = open(cwd+'/counters/%s.txt' % asin,'r')

		counterLog = fread.readline()
		counterLog = int(counterLog)


		if counterLog < 24:
			print counterLog
		else:
			# import pdb; pdb.set_trace()
			print counterLog
			print "Remove the item"
			endMyListing(itemid)	
			return False
		fread.close()

		fopen = open(cwd+'/counters/%s.txt' % asin, 'w')
		counterIter = counterLog + 1

		fopen.write('%d' % counterIter)
		fopen.close()
		flagEndListing = False

	def endMyListing(itemid):
		try:
			api = ebay_api.connectTradingReal()
			myitem = {
				"ItemID": i,
				"EndingReason": "Incorrect"
			}
			response = api.execute('EndFixedPriceItem', myitem)
			print "Listing with ItemID "+str(itemid)+" has ended because ASIN not found! \n"
			# import pdb; pdb.set_trace()
		except ConnectionError as e:
			print e.response.reply.Errors
			print "\n"



	def search_asin(asin,itemid):
		maxRetries = 0
		api = API(locale='uk')
		flag = True
		while flag == True:
			try:
				results = api.item_lookup(ItemId=asin,ResponseGroup='Offers')
				time.sleep(0.5)
				flag = False
				maxRetries = 0
				# import pdb; pdb.set_trace()

				try:
					fopen = open(cwd+'/data/output'+ str(asin) +'.xml','w')
					fopen.write(etree.tostring(results, pretty_print=True))
					fopen.close()
				except Exception as e:
					print e

			except Exception as e:
				print e
		
				if str(e) == "AWS.InvalidParameterValue: "+str(asin)+" is not a valid value for ItemId. Please change this value and retry your request.":
					print "Invalid Asin" 
					flagOut = checkInvalidAsin(asin,itemid)
					if flagOut == False:
						return str(100),1
				else:
					maxRetries += 1
					if maxRetries == 5:
						time.sleep(20.0)
					results = None
					time.sleep(2.0)
					flag = True


		finalprice = None
		'''
		try:
			finalprice = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
		except Exception as e:
			finalprice = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01
		'''
		flagPrice = False
		flagSalePrice = False
		flagEndListing = True




		try:
			finalprice = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
			flagSalePrice = True
		except Exception as e:
			# print e
			try:
				finalprice = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01
				flagPrice = True
			except Exception as e:
				print e
				priceExist = isinstance(finalprice, float)
				if priceExist == False and flagPrice == False and flagSalePrice == False:
					print "Listing with ASIN " +asin+" and itemID " +itemid +" is about to End. \n"
					

					#Counter log for ending listings
					try:
						fread = open(cwd+'/counters/%s.txt' % asin,'r')
					except:
						fopen = open(cwd+'/counters/%s.txt' % asin, 'w')
						counterIter = 0
						fopen.write('%d' % counterIter)
						fopen.close()

						
					fread = open(cwd+'/counters/%s.txt' % asin,'r')
					counterLog = fread.readline()
					counterLog = int(counterLog)


					if counterLog < 24:
						print counterLog
					else:
						# import pdb; pdb.set_trace()
						print counterLog
						print "Remove the item"
						endMyListing(itemid)	

					fread.close()

					fopen = open(cwd+'/counters/%s.txt' % asin, 'w')
					counterIter = counterLog + 1

					fopen.write('%d' % counterIter)
					fopen.close()
					flagEndListing = False

					#-------------

					#import pdb; pdb.set_trace()
		# try:
		# 	finalprice = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
		# except Exception as e:
		# 	finalprice = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01
		
		# else:
		# 	import pdb; pdb.set_trace()
		# 	endMyListing(itemid)
		# # print float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01, float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
		myflag = 0
		if results == None or finalprice==None:
			print "Final Price found as None"
			myflag = 1

			
		# results = api.item_lookup(ItemId=asin,ResponseGroup='Large')
			# time.sleep(15.0)
		# priceNormal = float(results.Items.Item.ItemAttributes.ListPrice.Amount)*0.01
		# priceOffer = float(results.Items.Item.OfferSummary.LowestNewPrice.Amount)*0.01
		# title = results.Items.Item.ItemAttributes.Title 
		# print title+ "\n"
		if flagEndListing == True:
			fopen = open(cwd+'/counters/%s.txt' % asin, 'w')
			counterIter = 0

			fopen.write('%d' % counterIter)
			fopen.close()


		available = True
		try:
			availability = results.Items.Item.Offers.Offer.OfferListing.Availability

			if str(availability) == "Usually dispatched within 1 to 2 months" or str(availability) == "Usually dispatched within 2 to 5 weeks" or str(availability) == "Usually dispatched within 1 to 4 weeks" or str(availability) == "Usually dispatched within 4-5 business days" or str(availability) == "Usually dispatched within 3 to 4 days" or str(availability) == "Temporarily out of stock. Order now and we'll deliver when available. We'll e-mail you with an estimated delivery date as soon as we have more information. Your credit card will not be charged until we ship the item.":
				print "Usually dispatched within 1 to 2 months"
				available = False
		except Exception as e:
			print e

		return str(finalprice),myflag,available
		
		
	shippingPrice_lst = []
	itemID_lst = []
	asin_lst = []
	buyit_lst = []
	qty_lst = []
	counterActiveList = 0
	flagFetchItems = True
	pagesCounter = 1
	while flagFetchItems:
		try:
			api = ebay_api.connectTradingReal()
			myitem = {
				"ActiveList": {
					"Include":"True",
						"Pagination":{
							"EntriesPerPage":"200",
							"PageNumber":str(pagesCounter)
					}
				}
			}

			response = api.execute('GetMyeBaySelling', myitem)
			pagesCounter += 1


			counter_items = 0
			for i in response.reply.ActiveList.ItemArray.Item:

				try:


					if "," in i.SKU:
						shipPrice = i.SKU.split(",")[1]
						shippingPrice_lst.append(shipPrice)
					else: 
						shippingPrice_lst.append("0")
					itemID_lst.append(i.ItemID)
					# import pdb;pdb.set_trace()
					if "," in i.SKU:
						asin_lst.append(i.SKU.split(",")[0])
					else:
						asin_lst.append(i.SKU)



					# print i.SKU
					# shipPrice = i.SKU.split(",")[1]
					# import pdb; pdb.set_trace()
					# shippingPrice_lst.append(shipPrice)
					# itemID_lst.append(i.ItemID)
					# asin_lst.append(i.SKU.split(",")[0])
					buyit_lst.append(i.BuyItNowPrice.value)
					# import pdb; pdb.set_trace()
					qty_lst.append(i.QuantityAvailable)
					counter_items +=1
					counterActiveList += 1
					# import pdb; pdb.set_trace()
				except Exception as e:
					# print e
					# pass
					continue
		except ConnectionError as e:
			print e.response.reply.Errors

		if counter_items != 200:
			flagFetchItems = False
	print "No. of Items found in Active List is: " + str(counterActiveList)
	# import pdb;pdb.set_trace()

	counterRetry = 1

	# counterRetry_lst = [x for x in xrange(18,20000,15)]
	for i,j,z,sh,qty in zip(itemID_lst,asin_lst,buyit_lst,shippingPrice_lst,qty_lst):
		print "No. of Items Checked: "+ str(counterRetry)
		# time.sleep(12.0)
		# if counterRetry < 40:
		# 	counterRetry +=1
		# 	continue
		# else:
		# 	counterRetry +=1

		counterRetry += 1

		priceOffer,myflag,avail = search_asin(j,i)
		if myflag == 1:
			continue


		if avail == True:
			itemavail = "3"

		if avail == False:
			itemavail = "0"


		draft_offer = priceOffer
		shippingPrice = float(sh)
		# priceOffer = ((float(priceOffer)+shippingPrice)*(0/100+1)+(((float(priceOffer)+shippingPrice)*(0/100+1)*5.8/100+0.3)+0.2+abs(((float(priceOffer)+shippingPrice)*(0/100+1)*5.8/100+0.3)-0.2))/2+0.2)/((100-9-3.4)/100)
		priceOffer = calculatePrice(float(priceOffer),shippingPrice)
		priceOffer = "%.2f" % priceOffer
		ebayPrice = z

		#Check Last Digit if zero
		if priceOffer[-1:] == '0':
			priceOffer= priceOffer[:-1]
		# print ebayPrice,priceOffer

		# import pdb; pdb.set_trace()
		if ebayPrice == priceOffer and qty == itemavail:
			print "No change in price detected"
			print datetime.datetime.now()
			print "ASIN: " + str(j) + "\n"
		else:
			print "Revising ItemID "+str(i)+ " with ASIN " + j
			print "Price: \nListed Price "+z+", Offer Amazon Price "+str(draft_offer)+ ", Ebay Price After Offer "+ str(priceOffer)+"\n"
			try:
				api = ebay_api.connectTradingReal()
				myitem = {
					"Item": {
						"ItemID": i,
						"PayPalEmailAddress": "reliablesales101@gmail.com",
						"StartPrice": priceOffer,
						"Quantity" : itemavail
					}
				}
				response = api.execute('ReviseFixedPriceItem', myitem)
				print datetime.datetime.now()
				print "Price Changed \n"
				# import pdb; pdb.set_trace()
			except ConnectionError as e:
				print e.response.reply.Errors
				print datetime.datetime.now()
				print "\n"

if __name__ == '__main__':
	execute_sku_grid()
