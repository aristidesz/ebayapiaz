# coding=utf-8
# encoding=utf8 
from amazonproduct import API
import os
import time
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
from lxml import etree
from add_item import addFixedPriceItem
from common import dump
import credentials_ebay as ce
import api_requests as ebay_api
from kzalez_API.Item import Item
from kzalez_API.ImageSelection import ImageSelection
import glob, os
import urllib
import random
import resize_images
import urllib2
from bs4 import BeautifulSoup


sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()



opener = urllib2.build_opener() 
opener.addheaders = [('User-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36')]

id = id
try:
	urlOrAssin=str(raw_input('Insert ASIN:'))
except ValueError:
	print "Error occurred!"

offerUrl = 'https://www.amazon.co.uk/gp/offer-listing/'
offerUrl += urlOrAssin
offerUrl += '/ref=dp_olp_new?ie=UTF8&condition=new'
url = offerUrl
response = opener.open(url)
page = response.read()
soup = BeautifulSoup(page, "html.parser")
# print soup
# running sets
# print price
suppliers = soup.find_all(class_='a-row a-spacing-mini olpOffer')
print suppliers
price = suppliers[0].find(class_='a-size-large a-color-price olpOfferPrice a-text-bold').text
if price:
	price=price.strip()
else:
	price="NA"
