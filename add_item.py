# coding=utf-8
# encoding=utf8 
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
import template
from common import dump
import credentials_ebay as ce
import api_requests as ebay_api
from kzalez_API.Item import Item
from kzalez_API.ImageSelection import ImageSelection
import glob, os,sys
import csvReader


sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()


def addFixedPriceItem(title,brand,feature_bullets,product_description,category_Id,final_price,images_url,asin,shippingprice,amazonTitle,initialPrice,mpnc):

	title = str(title)[:80]


	import sys  

	reload(sys)  
	sys.setdefaultencoding('utf8')

	final_price = "%.2f" % final_price


	bf = ""

	if feature_bullets!= "":
		for bu in feature_bullets:
			bu = "<li>" + str(bu) + "</li>"
			bf = bf + str(bu)
	
	feature_bullets = "<ul>" + bf + "</ul>"

	# print feature_bullets
	if len(images_url) > 1:
		main_image = images_url[0]
	else:
		main_image = images_url[0]
		print "One image detected!"
	
	product_description =  template.prod_desc(title,main_image,feature_bullets,product_description) 
	
	MPN = "N/A"
	csvReader.writeNewEanToDict(str(asin))
	mpn = csvReader.returnEanDict(str(asin))
	print brand,mpn
	

	myURLs = ','.join(map(str, images_url)) 
	



	try:
		api = ebay_api.connectTradingReal()
		

		myitem = {
			"Item": {
				"Title": str(title),
				"Description": str(product_description),
				"PrimaryCategory": {"CategoryID": str(category_Id)},
				"CategoryMappingAllowed": "true",
				"Country": "GB",
				"ConditionID": "1000",
				"Currency": "GBP",
				"StartPrice": str(final_price),
				"DispatchTimeMax": "1",
				"ListingDuration": "GTC",
				"ListingType": "FixedPriceItem",
				"PaymentMethods": "PayPal",
				"PayPalEmailAddress": "reliablesales101@gmail.com",
				"PostalCode":"SE14 5QY",
				"PictureDetails": {"PictureURL": images_url},
				"Quantity": "2",
				"SKU": asin+","+str(shippingprice),
				"PrivateNotes": str(shippingprice),
				"AutoPay": 'True',
				"ItemSpecifics":{
					"NameValueList":
					[
					  {
					    "Name": "Brand",
					    "Value": brand
					  },
					  {
					    "Name": "MPN",
					    "Value": mpnc
					  },
					  {
						  	"Name": "UPC",
						  	"Value": mpn
					  }
					]},
				"ProductListingDetails":{
					"EAN":mpn
				},
				"ReturnPolicy": {
					"ReturnsAcceptedOption": "ReturnsAccepted",
					# "RefundOption": "MoneyBack",
					"ReturnsWithinOption": "Days_30",
					"Description": "Return postage within 30 days.",
					"ShippingCostPaidByOption": "Buyer"
				},
				"ShippingDetails": {
					"ShippingType": "Flat",
					"ShippingServiceOptions": {
						"ShippingServicePriority": "1",
						"ShippingService": "UK_SellersStandardRate",
						"ShippingServiceCost": "0.00"
					}
				},
				"Site": "UK"
			}
		}

		response = api.execute('AddFixedPriceItem', myitem)

		myCsvRow = str(asin)+","+str(amazonTitle)+","+str(initialPrice)+","+str(title)+"\n"
		fd = open('Listings/MyListings.csv','a')
		fd.write(myCsvRow)
		fd.close()
	except ConnectionError as e:

		print e.response.reply.Errors


	for file in glob.glob("*.jpg"):

		os.remove(file)





def addFixedPriceItemBook(title,brand,feature_bullets,product_description,category_Id,final_price,images_url,asin,shippingprice,amazonTitle,initialPrice,mpnc,isbn):

	title = str(title)[:80]

	# import pdb; pdb.set_trace()
	import sys  

	reload(sys)  
	sys.setdefaultencoding('utf8')

	final_price = "%.2f" % final_price

	# product_description = product_description
	# print title,product_description,category_Id,final_price,images_url

	bf = ""

	if feature_bullets!= "":
		for bu in feature_bullets:
			bu = "<li>" + bu + "</li>"
			bf = bf + bu
	
	feature_bullets = "<ul>" + bf + "</ul>"

	# print feature_bullets
	if len(images_url) > 1:
		main_image = images_url[0]
	else:
		main_image = images_url[0]
		print "One image detected!"
	#
	product_description =  template.prod_desc(title,main_image,feature_bullets,product_description) 
	
	MPN = "N/A"
	csvReader.writeNewEanToDict(str(asin))
	mpn = csvReader.returnEanDict(str(asin))
	print brand,mpn
	

	myURLs = ','.join(map(str, images_url)) 
	

	try:
		api = ebay_api.connectTradingReal()
		
		

		myitem = {
			"Item": {
				"Title": str(title),
				"Description": str(product_description),
				"PrimaryCategory": {"CategoryID": str(category_Id)},
				"CategoryMappingAllowed": "true",
				"Country": "GB",
				"ConditionID": "1000",
				"Currency": "GBP",
				"StartPrice": str(final_price),
				"DispatchTimeMax": "1",
				"ListingDuration": "GTC",
				"ListingType": "FixedPriceItem",
				"PaymentMethods": "PayPal",
				"PayPalEmailAddress": "reliablesales101@gmail.com",
				"PostalCode":"SE14 5QY",
				"PictureDetails": {"PictureURL": images_url},
				"Quantity": "2",
				"SKU": asin+","+str(shippingprice),
				"PrivateNotes": str(shippingprice),
				"AutoPay": 'True',
				"ItemSpecifics":{
					"NameValueList":
					[
					  {
					    "Name": "Brand",
					    "Value": brand
					  },
					  {
					    "Name": "MPN",
					    "Value": mpnc
					  },
					  {
						  	"Name": "UPC",
						  	"Value": mpn
					  }
					]},
				"ProductListingDetails":{
					"EAN":mpn,
					"ISBN":isbn
				},
				"ReturnPolicy": {
					"ReturnsAcceptedOption": "ReturnsAccepted",
					# "RefundOption": "MoneyBack",
					"ReturnsWithinOption": "Days_30",
					"Description": "Return postage within 30 days.",
					"ShippingCostPaidByOption": "Buyer"
				},
				"ShippingDetails": {
					"ShippingType": "Flat",
					"ShippingServiceOptions": {
						"ShippingServicePriority": "1",
						"ShippingService": "UK_SellersStandardRate",
						"ShippingServiceCost": "0.00"
					}
				},
				"Site": "UK"
			}
		}

		response = api.execute('AddFixedPriceItem', myitem)


		myCsvRow = str(asin)+","+str(amazonTitle)+","+str(initialPrice)+","+str(title)+"\n"
		fd = open('Listings/MyListings.csv','a')
		fd.write(myCsvRow)
		fd.close()
	except ConnectionError as e:
		
		print e.response.reply.Errors


	for file in glob.glob("*.jpg"):

		os.remove(file)
