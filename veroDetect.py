# coding=utf-8
# encoding=utf8 
import datetime
from amazonproduct import API
import os
import time
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from ebaysdk.trading import Connection as Trading
from common import dump
import credentials_ebay as ce
import api_requests as ebay_api
import glob, os
from lxml import etree
import codecs
import string
from collections import Counter

sbAppId = ce.appCredentials.getSandBoxAppId()
sbDevId = ce.appCredentials.getSandBoxDevId()
sbCertId = ce.appCredentials.getSandBoxCertId()
sbRuName = ce.appCredentials.getSandBoxRuName()
prAppId = ce.appCredentials.getProductionAppId()
prDevId = ce.appCredentials.getProductionDevId()
prCertId = ce.appCredentials.getProductionCertId()
prRuName = ce.appCredentials.getProductionRuName()


serverUrl = '/home/ubuntu/ebay/stable_running/'

def execute_sku_grid():

	def getRestricedKeywords():
		keywords_lst = []
		fkeywords = open('./vero/keywords.txt','r')
		for line in fkeywords:
			line = line.strip()
			keyword = line.lower()
			keywords_lst.append(keyword)
			# import pdb;pdb.set_trace()
		return keywords_lst
	
	def search_asin(asin,itemid):
		maxRetries = 0
		api = API(locale='uk')
		flag = True
		print str(asin)
		while flag == True:
			try:
				results = api.item_lookup(ItemId=asin,ResponseGroup='Large')
				time.sleep(0.5)
				flag = False
				maxRetries = 0

			except Exception as e:
				print e
				pass

				maxRetries += 1
				if maxRetries == 5:
					time.sleep(20.0)
				results = None
				time.sleep(2.0)
				flag = True
				# import pdb; pdb.set_trace()
		
		finalprice = None

		flagPrice = False
		flagSalePrice = False
		flagEndListing = True




		try:
			finalprice = float(results.Items.Item.Offers.Offer.OfferListing.SalePrice.Amount)*0.01
			flagSalePrice = True
		except Exception as e:
			print e
			try:
				finalprice = float(results.Items.Item.Offers.Offer.OfferListing.Price.Amount)*0.01
				flagPrice = True
			except Exception as e:
				print e
				priceExist = isinstance(finalprice, float)
				if priceExist == False and flagPrice == False and flagSalePrice == False:
					print "Listing with ASIN " +asin+" and itemID " +itemid +" is about to End. \n"
					# exit()


					#-------------

		myflag = 0
		# if results == None or finalprice==None:
			# print "Final Price found as None"
			# myflag = 1

		try:
			url = results.Items.Item.DetailPageURL
		except Exception as e:
			exit()
		try:
			title = results.Items.Item.ItemAttributes.Title
		except Exception as e:
			title = None
		try:
			color = results.Items.Item.ItemAttributes.Color
		except Exception as e:
			color = None
		try:
			size = results.Items.Item.ItemAttributes.Size
		except Exception as e:
			size = None


			# url = BeautifulSoup(url,from_encoding="utf-8")
		# print str(title)
		# mystring = mystring.encode("utf-8")

		try:
			primeFlag = results.Items.Item.Offers.Offer.OfferListing.IsEligibleForPrime
		except Exception as e:
			primeFlag = 1
		

		return str(finalprice),myflag,url,color,size,primeFlag
		
		
		
	shippingPrice_lst = []
	itemID_lst = []
	asin_lst = []
	buyit_lst = []
	qty_lst = []
	title_lst = []
	description_lst = []
	counterActiveList = 0
	flagFetchItems = True
	pagesCounter = 1
	while flagFetchItems:
		try:
			api = ebay_api.connectTradingReal()
			myitem = {
				"ActiveList": {
					"Include":"True",
						"Pagination":{
							"EntriesPerPage":"200",
							"PageNumber":str(pagesCounter)
					},
					"Sort": "StartTime"
				}
			}

			response = api.execute('GetMyeBaySelling', myitem)
			pagesCounter += 1


			counter_items = 0
			for i in response.reply.ActiveList.ItemArray.Item:

				try:
					# print i.SKU
					if "," in i.SKU:
						shipPrice = i.SKU.split(",")[1]
						shippingPrice_lst.append(shipPrice)
					else: 
						shippingPrice_lst.append("0")
					itemID_lst.append(i.ItemID)
					# import pdb;pdb.set_trace()
					if "," in i.SKU:
						asin_lst.append(i.SKU.split(",")[0])
					else:
						asin_lst.append(i.SKU)
					title_lst.append(i.Title)
					buyit_lst.append(i.BuyItNowPrice.value)
					# import pdb; pdb.set_trace()
					qty_lst.append(i.QuantityAvailable)
					counter_items +=1
					counterActiveList += 1
					# import pdb; pdb.set_trace()
				except Exception as e:
					# print e
					# pass
					continue
		except ConnectionError as e:
			print e.response.reply.Errors


		# print counter_items
		if counter_items != 200:
			flagFetchItems = False
	print "No. of Items found in Active List is: " + str(counterActiveList)
	

	# # asin_lst = []
	today = datetime.date.today()

	fVERO = open('./vero/VEROReport'+str(today)+'.dat','w')
	keywords = getRestricedKeywords()
	# for q,w,e,r,asin in zip(url_lst,size_lst,color_lst,itemID_lst,asin_lst):
	counterVERO = 0
	counter_lst = []
	for r,asin,title in zip(itemID_lst,asin_lst,title_lst):
		# print r,title
		title = title.lower()
		for word in keywords:
			if word in title:
				print r+","+title+","+word
				fVERO.write(r+","+title.encode('utf-8').strip()+","+word.encode('utf-8').strip()+"\n")
				counter_lst.append(word)
				counterVERO += 1

	print Counter(counter_lst)
	fVERO.write(str(Counter(counter_lst)))
	print "Number of possible VERO problems: "+ str(counterVERO)



if __name__ == '__main__':
	execute_sku_grid()
